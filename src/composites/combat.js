import { assign } from '../util/objecty';
import {ref, shallowRef, triggerRef} from 'vue';

import Events, {
	ALLY_DIED,
	CHAR_ACTION,
	CHAR_DIED,
	COMBAT_WON,
	DAMAGE_MISS,
	DOT_ACTION, DOT_EXPIREACTION,
	ENEMY_SLAIN,
	EVT_COMBAT,
	ITEM_ACTION,
	STATE_BLOCK
} from '../events';

import { itemRevive } from '../modules/itemgen';

import {Npc} from '../chars/npc';
import game from '../game';
import {
	ApplyAction,
	PrimeInd,
	PrimeTarget,
	RandTarget,
	TARGET_ALLY,
	TARGET_ANY,
	TARGET_ENEMY,
	TARGET_GROUP,
	TARGET_NONPRIMARY,
	TARGET_NOTSELF,
	TARGET_PRIMARY,
	TARGET_RAND,
	TARGET_SELF
} from "../values/combatVars";
import { COMPANION, TEAM_ALL, TEAM_NPC, TEAM_PLAYER, WEAPON, canTarget, getDelay } from '../values/consts';


export default class Combat {

	get id() { return 'combat' }

	toJSON() {

		let a = undefined;
		const allies = this.allies;
		if (allies.length > 1) {

			a = [];
			for (let i = 1; i < allies.length; i++) {
				const v = allies[i];
				a.push(v.keep ? v.id : v)
			}

		}

		return {
			enemies: this.enemies,
			allies: a
		}

	}

	/**
	 * Whether combat is active.
	 * @property {boolean} active
	 */
	get active() { return this._active; }
	set active(v) { this._active = v }

	_enemies = shallowRef([]);
	/**
	 * @property {Npc[]} enemies - enemies in the combat.
	 */
	get enemies() { return this._enemies.value; }
	set enemies(v) { this._enemies.value = v; }

	_allies = shallowRef([]);
	/**
	 * @property {Char[]} allies - player & allies. allies[0] is always the player.
	 */
	get allies() { return this._allies.value; }
	set allies(v) { this._allies.value = v; }

	/**
	 * @property {boolean} done
	 */
	get done() { return this.enemies.length === 0; }

	/**
	 * @property {Char[][]} teams - maps team id to team list.
	 */
	get teams() { return this._teams; }

	constructor(vars = null) {

		if (vars) assign(this, vars);

		this.active = false;

		this._teams = [];

	}

	/**
	 *
	 * @param {GameState} gs
	 */
	revive(gs) {

		this.state = gs;
		this.player = gs.player;
		// splices done in place to not confuse player with changed order.

		const enemies = this.enemies;
	
		let it;

		for (let i = enemies.length - 1; i >= 0; i--) {

			// data can be null both before and after itemRevive()
			it = enemies[i];
			if (it) {
				it = enemies[i] = itemRevive(gs, it);
			}
			if (!it || !(it instanceof Npc)) {
				enemies.splice(i, 1);
			}


		}

		const allies = this.allies;
		for (let i = allies.length - 1; i >= 0; i--) {

			it = allies[i];
			if (typeof it === 'string') allies[i] = gs.minions.find(it);
			else if (it && typeof it === 'object' && !(it instanceof Npc)) {
				allies[i] = itemRevive(gs, it);
			}

			if (!allies[i]) allies.splice(i, 1);

		}

		allies.unshift(this.player);
		//this.enemies = [...this.enemies];

		this.resetTeamArrays();

		Events.add(ITEM_ACTION, this.itemAction, this);
		Events.add(CHAR_ACTION, this.spellAction, this);
		Events.add(DOT_ACTION, this.dotAction, this);
		Events.add(DOT_EXPIREACTION, this.dotExpireAction, this);
		Events.add(CHAR_DIED, this.charDied, this);

	}

	begin() {
		const enemies = this.enemies;
		for(const enemy of enemies) {
			if(enemy.begin instanceof Function) enemy.begin();
		}
	}

	update(dt) {

		if (this.player.alive === false) return;

		let e, action;
		for (let i = this.allies.length - 1; i >= 0; i--) {

			e = this.allies[i];

			if (i > 0) {
				// non-player allies.
				if (e.alive === false) {
					e.deathThroes();
					this.allies.splice(i, 1);
					continue;
				}
				
				e.update(dt);

				if ( e.alive === false ) {
					e.deathThroes();
					this.allies.splice(i,1);
					continue;
				}
			}

			action = e.combat(dt);
			if (!action) continue;

			else if (!action.canAttack()) {
				Events.emit(STATE_BLOCK, e, action);
			} else this.attack(e, action);

		}

		for (let i = this.enemies.length - 1; i >= 0; i--) {

			e = this.enemies[i];

			// Checked before, because the enemy could've died from an attack, and update could cause it to heal after dying.
			if ( e.alive === false ) {
				e.deathThroes();
				this.enemies.splice(i,1);
				if ( this.enemies.length === 0 ) Events.emit( COMBAT_WON );
				continue;
			}

			e.update(dt);

			// Checked after, to see if it died after updates, possibly due to dots.
			if (e.alive === false) {
				e.deathThroes();
				this.enemies.splice(i, 1);
				if (this.enemies.length === 0) Events.emit(COMBAT_WON);
				continue;
			}

			action = e.combat(dt);
			if (!action) continue;

			else if (!action.canAttack()) {
				Events.emit(STATE_BLOCK, e, action);
			} else this.attack(e, action);

		}


	}

	/**
	 * Player-casted spell or action attack.
	 * @param {Item} it
	 * @param {Context} g
	 */
	spellAction(it, g) {
		//first we check if the action has any caststoppers - aka conditions that would prevent it. If it does, we check if we have any of those conditions, and if we have even 1, gg.
		let a
		if (it.caststoppers) {
			for (let b of it.caststoppers) {
				a = g.self.getCause(b);
				if (a) break;
			}
		}
		if (a) {
			Events.emit(STATE_BLOCK, g.self, a);

		} else {

			//Events.emit(EVT_COMBAT, null, g.self.name + ' casts ' + it.name + ' at the darkness.' );
			//This capitalizes all the spells in the combat log.
			Events.emit(EVT_COMBAT, g.self.name.toTitleCase() + ' Casts ' + it.name.toTitleCase());
			if (it.attack) {
				this.attack(g.self, it.attack);
			}
			if (it.action) {

				const target = this.getTarget(g.self, it.action.targets, it.action.targetstats);

				if (!target) return;
				if (Array.isArray(target)) {

					for (let i = target.length - 1; i >= 0; i--) ApplyAction(target[i], it.action, g.self);

				} else {
					ApplyAction(target, it.action, g.self);
				}


			}
		}

	}
	/**
	 * item-casted spell or action attack.
	 * @param {Item} it
	 * @param {Context} g
	 */
	itemAction(it, g) {


		Events.emit(EVT_COMBAT, null, g.self.name + ' Uses ' + it.name.toTitleCase());
		if (it.use.attack) {
			this.attack(g.self, it.use.attack);

		}
		if (it.use.action) {

			let target = this.getTarget(g.self, it.use.action.targets, it.use.action.targetstats);

			if (!target) return;
			if (Array.isArray(target)) {

				for (let i = target.length - 1; i >= 0; i--) ApplyAction(target[i], it.use.action, g.self);

			} else {
				ApplyAction(target, it.use.action, g.self);
			}


		}


	}
	//for use by dots
	dotAction(it, g) {

		if (it.attack) {
			this.attack(g.self, it.getAttack());
		}
		if (it.action) {

			let target = this.getTarget(g.self, it.action.targets, it.action.targetstats);

			if (!target) return;
			if (Array.isArray(target)) {

				for (let i = target.length - 1; i >= 0; i--) ApplyAction(target[i], it.action, g.self);

			} else {
				ApplyAction(target, it.action, g.self);
			}
		}

	}
	dotExpireAction(it, g) {
		this.attack(g.self, it);
	}
	/**
	 * Attack a target.
	 * @param {Char} attacker - enemy attacking.
	 * @param {Object|Char} atk - attack object.
	 */
	attack(attacker, atk) {
		if (atk.log) {
			Events.emit(EVT_COMBAT, null, atk.log);
		}

		if (atk.hits) {

			for (let p in atk.hits) {
				this.attack(attacker, atk.hits[p]);
			}
		}

		let targ = this.getTarget(attacker, atk.targets, atk.targetstats);
		if (!targ) return;
		for (let a = 0; a < (atk.repeathits || 1); a++) {
			if (Array.isArray(targ)) {

				for (let i = targ.length - 1; i >= 0; i--) {
					if (!atk.only || canTarget(atk.only, targ[i])) {
						this.doAttack(attacker, atk, targ[i]);
					}
				}

			} else {
				if (!atk.only || canTarget(atk.only, targ)) {
					this.doAttack(attacker, atk, targ);
				}
			}
		}

	}

	/**
	 *
	 * @param {Char} attacker
	 * @param {Attack} atk
	 * @param {Char} targ
	 */
	doAttack(attacker, atk, targ) {

		if (!targ || !targ.alive) return;
		// attack automatically hits if it's harmless, target is defenseless OR in defensive stance AND dodge roll fails.
		if (atk.harmless || !targ.canDefend() || this.tryHit(attacker, targ, atk) || targ.canParry()) {

			ApplyAction(targ, atk, attacker, this.tryParry(attacker, targ, atk));

		}

	}

	/**
	 * @param {Char} char
	 * @param {string} targets
	 * @returns {Char|Char[]|null}
	 */
	getTarget(char, targets, targetstats = null) {
		// retarget based on state.
		targets = char.retarget(targets);

		const group = this.getGroup(targets, char.team);

		let filtergroup = Array.from(group)
		// Get id for ally leader (relative to attacker)
		let allylead = this.allies[PrimeInd(this.allies)].id

		if (!this.active) {

			if (group === this.enemies) return null;
			if (group === this.teams[TEAM_ALL]) {
				filtergroup = Array.from(this.allies)

				if (targets & TARGET_PRIMARY) {
					filtergroup.splice(1);
					return filtergroup;
				}

				if (targets & TARGET_NONPRIMARY) {
					filtergroup.splice(filtergroup.map(e => e.id).indexOf(allylead), 1)
				}

				if (targets & TARGET_NOTSELF) {
					let attackerIndex = filtergroup.map(e => e.id).indexOf(char.id)
					if (attackerIndex > -1) {
						filtergroup.splice(attackerIndex, 1)
					}
				}

				return filtergroup
			}
		}
		let enemylead = undefined
		if (this.enemies[PrimeInd(this.enemies)]) {
			enemylead = this.enemies[PrimeInd(this.enemies)].id
		}

		if (targets & TARGET_GROUP) {

			// Handling for "group" + "primary" + "any" condition aka "bothleaders"
			if ((targets & TARGET_PRIMARY) && (group === this.teams[TEAM_ALL])) {
				filtergroup = [this.allies[0], this.enemies[0]];
				return filtergroup;
			}

			if (targets & TARGET_NONPRIMARY) {
				if (group !== this.teams[TEAM_ALL]) {
					filtergroup = filtergroup.slice()
					filtergroup.splice(0, PrimeInd(group) + 1)
				} else {
					filtergroup.splice(filtergroup.map(e => e.id).indexOf(allylead), 1)
					filtergroup.splice(filtergroup.map(e => e.id).indexOf(enemylead), 1)
				}
			}

			if (targets & TARGET_NOTSELF) {
				let attackerIndex = filtergroup.map(e => e.id).indexOf(char.id)
				if (attackerIndex > -1) {
					filtergroup.splice(attackerIndex, 1)
				}
			}
			return filtergroup;
		}

		if (!targets || (targets & TARGET_RAND && !(targets & TARGET_GROUP))) {
			let ignoretaunt = false;

			if (targets && !(targets & TARGET_ENEMY)) ignoretaunt = true;
			if (targets & TARGET_NONPRIMARY) {
				let allyleadIdx = filtergroup.map(e => e.id).indexOf(allylead)
				if (allyleadIdx > -1) {
					filtergroup.splice(allyleadIdx, 1)
				}

				let enemyleadIdx = filtergroup.map(e => e.id).indexOf(enemylead)
				if (enemyleadIdx > -1) {
					filtergroup.splice(enemyleadIdx, 1)
				}

			}

			if (targets & TARGET_NOTSELF) {
				let attackerIndex = filtergroup.map(e => e.id).indexOf(char.id)
				if (attackerIndex > -1) {
					filtergroup.splice(attackerIndex, 1)
				}
			}

			// Handling for "random primary" condition aka "randomleader"
			if (targets & TARGET_PRIMARY) {
				filtergroup = [this.allies[0], this.enemies[0]];
			}

			return RandTarget(filtergroup, ignoretaunt, targetstats);
		} else if (targets & TARGET_SELF) return char;

		if (targets & TARGET_PRIMARY) return PrimeTarget(group);

	}

	/**
	 * Get the Char group to which the target flags can apply.
	 * Null or zero targets are assumed an enemy target.
	 * @param {number} targets
	 * @param {number} team - ally team.
	 */
	getGroup(targets, team) {

		if (!targets || (targets & TARGET_ENEMY)) {

			return team === TEAM_PLAYER ? this.enemies : this.allies;

		} else if (targets & (TARGET_ALLY + TARGET_SELF)) {
			return this.teams[team];

		} else if (targets & TARGET_ANY) return this.teams[TEAM_ALL];
		else if (targets & TARGET_RAND) {
			return Math.random() < 0.5 ? this.allies : this.enemies;
		}
		return null;
	}

	/**
	 * Rolls an attack roll against a defender.
	 * @param {Char} attacker - attack object
	 * @param {Char} defender - defending char.
	 * @param {Object} attack - attack or weapon used to hit.
	 * @returns {boolean} true if char hit.
	 */
	tryHit(attacker, defender, attack) {

		let tohit = attacker.getHit(attack.kind || null, (attack?.source?.type == WEAPON || attack?.source?.school == "martial"));
		if (attack && (attack != attacker)) tohit += (attack.tohit || 0);

		if (this.dodgeRoll(defender.dodge, tohit) && !attack.nododge) {
			if (attack.name) {
				Events.emit(DAMAGE_MISS, defender.name.toTitleCase() + ' Dodges ' + (attack.name.toTitleCase()));

			}
			else {
				Events.emit(DAMAGE_MISS, defender.name.toTitleCase() + ' Dodges ' + (attacker.name.toTitleCase()));
			}

		} else return true

	}

	tryParry(attacker, defender, attack) {
		if (!attack.noparry && defender.canParry()) {
			let tohit = attacker.getHit(attack.kind || null, (attack?.source?.type == WEAPON || attack?.source?.school == "martial"));
			if (attack && (attack != attacker)) tohit += (attack.tohit || 0);
			return this.dodgeRoll(Math.pow(defender.defense, 0.7), tohit)
		}
		else return false
	}

	/**
	 *
	 * @param {Npc[]} enemies
	 */
	setEnemies(enemies) {

		this.enemies = enemies;
		if (enemies.length > 0) {

			if (enemies[0]) Events.emit(EVT_COMBAT, enemies[0].name.toTitleCase() + ' Encountered');

		}

		this.resetTeamArrays();
		this.setTimers();

	}

	/**
	 * Add Npc to combat
	 * @param {Npc} it
	 */
	addNpc(it) {

		it.timer = getDelay(it.speed);

		if (it.team === TEAM_PLAYER) {
			this.allies.push(it);
			triggerRef(this.allies);
		} else {
			this.enemies.push(it);
			triggerRef(this.enemies);
		}

		this.teams[TEAM_ALL].push(it);

	}

	resetTeamArrays() {

		this.teams[TEAM_PLAYER] = this.allies;
		this.teams[TEAM_NPC] = this.enemies;
		this.teams[TEAM_ALL] = this.allies.concat(this.enemies);

	}

	/**
	 * Reenter a dungeon.
	 */
	reenter() {

		this.allies = this.state.minions.allies.toArray();
		let comp = this.state.getSlot(COMPANION)
		if (comp) {
			if (comp.onCreate) comp.onCreate(game, TEAM_PLAYER, false)
		}
		this.allies.unshift(this.player);
		this.resetTeamArrays();

	}

	/**
	 * Begin new dungeon.
	 */
	reset() {

		this.enemies.splice(0, this.enemies.length);
		this.reenter();

	}


	/**
	 * readjust timers at combat start to the smallest delay.
	 * prevents waiting for first attack.
	 */
	setTimers() {

		let minDelay = getDelay(this.player.speed);

		let t;
		for (let i = this.enemies.length - 1; i >= 0; i--) {
			t = this.enemies[i].timer = getDelay(this.enemies[i].speed);
			if (t < minDelay) minDelay = t;
		}
		for (let i = this.allies.length - 1; i >= 1; i--) { // >= 1 excludes player from this consideration. Let the player keep their delay, if any.
			t = this.allies[i].timer = getDelay(this.allies[i].speed);
			if (t < minDelay) minDelay = t;
		}


		// +1 initial encounter delay. Excludes player.
		minDelay -= 1;

		for (let i = this.enemies.length - 1; i >= 0; i--) {
			this.enemies[i].timer -= minDelay;
		}
		for (let i = this.allies.length - 1; i >= 1; i--) {
			this.allies[i].timer -= minDelay;
		}

	}

	/**
	 * @param {number} dodge
	 * @param {number} tohit
	 * @returns {boolean} true if defender dodges.
	 */
	dodgeRoll(dodge, tohit) {

		//let sig = 1 + (dodge-tohit)/( 1+ Math.abs(dodge-tohit));
		//let sig = 1 + (2/Math.PI)*( Math.atan(dodge-tohit) );
		//new attempt:
		/*
		let high = Math.max(Math.abs(tohit),Math.abs(dodge));
		if ( high == 0 ) {
			let a = 0.25 - Math.random()
			return a > 0 ? a : false
		};
		*/
		let sig = Math.min(0.95, (9 + Math.pow(Math.max(0, (dodge - tohit) + 14.7), 2)) / 900); // chance to dodge or parry

		let a = sig - Math.random()
		return a > 0 ? a : false;

	}

	charDied(char, attacker) {

		if (char === this.player) return;
		else if (char.team === TEAM_PLAYER) {

			Events.emit(ALLY_DIED, char);

		} else Events.emit(ENEMY_SLAIN, char, attacker);

	}

	getMonsters(id, team) {
		let monsters = []
		if (team === TEAM_PLAYER) {
			for (let i = 0; i < this.allies.length; i++) {
				if (this.allies[i].template) {
					if (this.allies[i]?.template.id == id && this.allies[i].alive == true) {
						monsters.push(this.allies[i])
					}
				}
			}
		} else {
			for (let i = 0; i < this.enemies.length; i++) {
				if (this.enemies[i]?.template.id == id && this.enemies[i].alive == true) {
					monsters.push(this.enemies[i])
				}
			}
		}
		return monsters;
	}

}
