import Attack from '../chars/attack';
import { ParseTarget, processDot } from "../values/combatVars";
import { GData } from "./gdata";

const defaults = {

	level:1,
	repeat:true,
	stack:true,
	consume:true

};

/**
 * This is actually only the prototype for a potion.
 * Individual potions are instanced from this data.
 */
export class Potion extends GData {

	get isRecipe() {return true; }

	constructor(vars=null ) {

		super(vars, defaults );

		if ( this.use.attack != null ) {
			this.use.attack = (this.use.attack instanceof Attack) ?
				this.use.attack :
				new Attack(this.use.attack );
			
		};
		if (this.use.action != null) {
			if(this.use.action.targets){
				this.use.action.targetstring = this.use.action.targets;
				this.use.action.targets = ParseTarget(this.use.action.targets);
			}
		};
		if (this.use.dot != null) {
			processDot(this.use.dot)
		};
		this.require = this.require||this.unlockTest;

	}

	/**
	 * Potions have this, but do nothing with it.
	 * @param {Context} g
	 */
	onUse( g ) {


	}

	/**
	 * Perform cd timer tick.
	 * @param {number} dt - elapsed time.
	 * @returns {boolean} true if timer is complete.
	 */
	tick(dt) {

		this.timer -= dt;

		if (this.timer < 0 ) {
			this.timer = 0;
			return true;

		}
		return false;

	}


}
