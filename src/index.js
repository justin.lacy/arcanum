import {createApp, h} from 'vue';
import Game from '@/game';
import { appEvents } from './events';
import DataLoader from './dataLoader';
import Profile from './modules/profile';
import Main from 'ui/main.vue';

const vm = createApp({
	components:{
		Main
	},
	data(){
		// hacky re-render force. used to rerender on game reload.
		return {
			active:Profile.active,
			renderKey:1
		}
	},
	created(){

		this.saveLink = null;

		//this.tryAutoLogin();

		appEvents.on('save-file', this.saveFile, this );
		appEvents.on('hall-file', this.hallFile, this );

		appEvents.on('load-file', this.loadFile, this );
		appEvents.on('load', this.loadSave, this );
		appEvents.on('reset', this.reset,this );
		appEvents.on('resetHall', this.resetHall, this );

		appEvents.on('stat', this.doStat, this );

		appEvents.on('save-settings', Profile.saveSettings, Profile );

		appEvents.on('set-char', this.setChar, this );
		appEvents.on('dismiss-char', this.dismissChar, this );

		appEvents.on('save', this.manualSave, this );
		appEvents.on('autosave', this.save, this );

		appEvents.on( 'setting', this.onSetting, this );
		appEvents.on( 'logout', this.logout, this );
		appEvents.on( 'login-changed', this.onLoginChanged, this );
		//this.on( 'try-register', this.tryRegister, this );

		DataLoader.requestData().then(() => this.loadHall());

	},
	methods:{

		onLoginChanged( loggedIn ){

			Profile.loggedIn = loggedIn;

			if ( loggedIn ) {
				this.loadHall('remote');
			}

		},

		logout(){
			Profile.logout();
		},

		tryLogin(uname, pw) {
		},

		loadHall( type=null ){

			//if ( forceClear ) this.reset();

			console.log('loading hall type: ' + type );
			appEvents.emit('pause');
			Profile.loadHall( type ).then( ()=>this.loadSave() );


		},

		doStat( evt, val ) {
		},

		/**
		 * Set current character.
		 */
		setChar( ind ){

			Profile.setActive( ind, Game.state );
			this.loadSave();

		},

		dismissChar(ind) {

			//console.log('DISMISS: ' + ind );
			Profile.dismiss( ind );

		},

		/**
		 * Load the save for the active wizard.
		 */
		async loadSave() {

			try {

				appEvents.emit('pause');

				const data = await Profile.loadActive();
				this.setStateJSON( data );

			} catch (e ) { console.error( e.message + '\n' + e.stack ); }

		},

		/**
		 * Game was revived from JSON.
		 * @param {Game} game
		 */
		gameLoaded( game ) {

			this.settingsLoaded( Profile.loadSettings() );
				
			appEvents.emit( 'game-loaded', game );

			console.log('loaded player: ' + game.state.pid );

			Profile.gameLoaded( game );

			appEvents.emit('unpause');

			this.renderKey++;

		},

		/**
		 * Call on settings loaded.
		 * @param {*} vars
		 */
		settingsLoaded(vars){

			if (!vars) return;

			this.onSetting( 'darkMode', vars.darkMode );
			this.onSetting( 'compactMode', vars.compactMode );
		},

		onSetting( setting, v ) {

			if ( setting === 'darkMode') {

				if ( v ) document.body.classList.add( 'darkmode');
				else document.body.classList.remove( 'darkmode');

			} else if ( setting === 'compactMode' ) {

				if ( v ) document.body.classList.add( 'compact');
				else document.body.classList.remove( 'compact');

			} else if ( setting === 'remoteFirst') {

				Profile.remoteFirst = true;
			}


		},

		/**
		 *
		 * @param {*} e
		 * @returns {void}
		 */
		saveFile(e ){

			// event shouldnt be null but sometimes is.
			if (!e )return;
			try {

				e.preventDefault();
				this.makeLink( Game.state, Game.state.player.name );

			} catch(ex) {
				console.error( ex.message + '\n' + ex.stack );
			}

		},

		/**
		 *
		 * @param {*} e
		 * @returns {void}
		 */
		async hallFile(e){

			if ( !e ) return;

			this.save();		// save game first.

			try {
				e.preventDefault();

				if ( this.hallLink) URL.revokeObjectURL( this.hallLink );

				const data = await Profile.getHallSave();
				this.makeLink( data, 'hall');

			} catch(ex){
				console.error( ex.message + '\n' + ex.stack );
			}

		},

		/**
		 * Create URL link for data.
		 * @param {object} data
		 * @returns {DOMString}
		 */
		makeLink( data, saveName ) {

			if ( this.saveLink ) URL.revokeObjectURL( this.saveLink );

			const json = JSON.stringify(data);
			const file = new Blob( [json], {type:"text/json;charset=utf-8"} );

			const a = document.createElement( 'a');
			//targ.type = 'text/json';
			a.download = a.title = saveName + '.json';

			this.saveLink = URL.createObjectURL( file );
			a.href = this.saveLink;
			a.click();

		},

		/**
		 * Load file from data.
		 * @param {*} files
		 */
		loadFile(files) {

			const file = files[0];
			if ( !file) return;

			appEvents.emit('pause');


			const reader = new FileReader();
			reader.onload = (e)=>{

				try {

					let data = JSON.parse( e.target.result );
					if ( data.type ==='hall') {

						this.setHallJSON( data );

					} else {

						this.setStateJSON( data );

					}


				} catch(err){
					console.error(  err.message + '\n' + err.stack );
				}

			}
			reader.readAsText( file );

		},

		/**
		 * Set JSON for complete hall-file with all associated wizards.
		 * @param {object} data
		 */
		async setHallJSON( data ) {

			await Profile.setHallSave( data );
			this.loadHall();	// load the hall data back. bit wasteful but organized.

		},

		/**
		 * Set wizard-state game data in the form of a json
		 * text string.
		 * @param {object} obj - raw game data.
		 */
		setStateJSON( obj=null ){

			try {

				//if ( Game.loaded ) this.renderKey++;
				this.renderKey++;

				Game.load( obj, Profile.getHallItems() ).then(

					this.gameLoaded,
					(e)=>{
						console.error(`GAME LOAD FAILED:`);
						console.error( e.message + '\n' + e.stack )
					 } );

			} catch( err ) {
				console.error(  err.message + '\n' + err.stack );
			}
		},

		manualSave(){
			this.save();
		},

		async save() {

			if (!Game.loaded ) return;
			const charsave = await Profile.saveActive( Game.state );
			if ( charsave ) {

				Profile.saveHall();

			} else{
				alert('ERR ON SAVE. NO SAVE.');
			} 

		},

		reset(){

			appEvents.emit('pause');

			Profile.clearActive();
			this.setStateJSON(null);


		},
		async resetHall() {

			appEvents.emit('pause');

			await Profile.clearAll();

			this.loadHall();

		}

	},
	render() {
		return h( Main, { key:this.renderKey, game:Game } );
	}

});

vm.mount("#vueRoot");