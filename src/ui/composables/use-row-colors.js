import { watch, ref } from "vue";
import {useEventListener} from "@vueuse/core";


/**
 * Apply row coloring classes to grid rows.
 * @param {Ref<HTMLElement} elRef - element containing the grid.
 * @param {string} rowClass - class to apply to odd-row grid elements
 */
export const useRowColors = (elRef, rowClass='odd-row')=>{

	const rootStyle = window.getComputedStyle(document.documentElement);
	const baseFontSize = parseFloat(rootStyle.fontSize);
	
	/// assuming grid is 12rem. TODO: compute browser root font size.
	const cellWidth = baseFontSize*12;

    const cols = ref(1);
    
    const onResize = ()=>{

        const el = elRef.value;
        
        if ( el ) {
            cols.value = Math.floor( el.offsetWidth / cellWidth );
        } else {
            cols.value = 1;
        }

        /*const len = el.children.length;
        for( let i = 0; i <len; i++){

            const child = el.children[i];
            if ( i % 2*cols < cols ) {
                child.classList.remove(rowClass);
            } else {
                child.classList.add(rowClass)
            }

        }*/


    }

    useEventListener( window, 'resize', onResize );
    watch(elRef, onResize );

    return cols;

}