import vue from '@vitejs/plugin-vue';
import fs from "fs";
import { resolve } from 'path';
import { defineConfig } from 'vite';
import { createHtmlPlugin } from "vite-plugin-html";
import { viteStaticCopy } from 'vite-plugin-static-copy';

const getApiBase = (url?: string) => {

  if (url == null) {
    return 'localhost';
  }
  const portIndex = url.lastIndexOf(':');
  return portIndex > 0 ? url.substring(0, portIndex) : url;

}

let packData: any = fs.readFileSync(
  'package.json', { encoding: 'utf-8' }
);
packData = JSON.parse(packData);
packData.version = "unstable " + packData.version;


// https://vitejs.dev/config/
export default defineConfig(async ({ mode, command }) => {

  console.log(`mode: ${mode}  command: ${command}`);

  const VERS_STR = JSON.stringify(packData.version);

  const isProduction = mode === 'production';
  const api_base = getApiBase(process.env.VITE_API_BASE);

  const securityPolicies = [
    "default-src 'self'",
    "connect-src 'self' " +
    [
      api_base,
    ].join(" "),
    "font-src 'self' data:",
    isProduction ? "style-src 'self'" : "style-src 'self' 'unsafe-inline' 'unsafe-eval'",
  ];

  return {
    base: './',
    resolve: {
      extensions: ['.js', '.ts', '.vue', '.json'],
      alias: {
        "@/": `${resolve(__dirname, './src/')}/`,
        "ui/": `${resolve(__dirname, './src/ui')}/`,
        "data/": `${resolve(__dirname, '../data')}/`,
        "remote": `${resolve(__dirname, './src/remote')}/`,
        "modules/": `${resolve(__dirname, './src/modules')}/`,
      }
    },

    build: {

      rollupOptions: {
        treeshake: true,
        output: {

        }
      }

    },
    define: {
      __VERSION: VERS_STR,
      __DEBUG: true,
      __CHEATS: true,
      __DIST: mode === 'production',
    },

    plugins: [

      createHtmlPlugin({
        inject: {
          data: {
            contentSecurityPolicy: securityPolicies.join("; "),
          },
        },
      }),
      vue(),
      viteStaticCopy({
        targets: [
          {
            src: 'data',
            dest: './'
          }
        ]
      }),
    ],

    optimizeDeps: {

      include: [
        "vue",
        "@vueuse/core",

      ]
    },

    preview: {
      port: 5173,
      cors: true,
    },

    server: {
      port: 5173,
      cors: true,
      hmr: {
        host: 'localhost',
      },
      watch: {
        ignored: [
          resolve(__dirname, "src/components.d.ts")
        ],
      },
    }

  };
});
